# devops-school-01

## Задание

1. Запаковать в докер код разработчика https://html5up.net/directive
2. Опубликовать в докерхаб
3. Исходный код положить в gitlab
4. Всю разработку вести в ветке dev, периодически мёржить в main по мере публикации образа в докерхаб.
5. В ветке main должны быть инструкция по запуску образа в формате Markdown

Для проверки задания прислать имя образа и ссылка на публичную репу

## Результат

### Dockerfile
```Dockerfile
FROM nginx
COPY ./frontend /usr/share/nginx/html
```
### Образ
* dim22/devops-school-01  - образ выложен на https://hub.docker.com/repository/docker/dim22/devops-school-01/

### Команды
Сборка образа
```bash
docker build . -t dim22/devops-school-01
```

Запуск образа
```bash
docker run -d -p 8080:80 dim22/devops-school-01
```